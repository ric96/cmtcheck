#!/bin/bash

OPCOUNT=20000

echo "Testing Single Core Int64 Perf:"
sint=$(\time -f "%e" stress-ng --cpu 1 --cpu-ops $OPCOUNT --cpu-method int64 -q 2>&1)

echo "Testing All Core Int64 Perf:"
aint=$(\time -f "%e" stress-ng --cpu $(nproc) --cpu-ops $OPCOUNT --cpu-method int64 -q 2>&1)

echo "Testing Single Core FloatDiv Perf:"
sfloat=$(\time -f "%e" stress-ng --fp 1 --fp-ops $OPCOUNT --fp-method floatdiv -q 2>&1)

echo "Testing All Core FloatDiv Perf:"
afloat=$(\time -f "%e" stress-ng --fp $(nproc) --fp-ops $OPCOUNT --fp-method floatdiv -q 2>&1)

intfact=$(echo "scale=2; $sint / $aint" | bc)
floatfact=$(echo "scale=2; $sfloat / $afloat" | bc)

echo "All Core Int Perf is faster by a factor of:"
echo "$intfact"

echo "All Core FP Perf is faster by a factor of:"
echo "$floatfact"

echo "CMT Check Score:"
echo "scale=2; $floatfact / $intfact" | bc